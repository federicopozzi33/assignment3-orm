# Assignment 3 - ORM

Develop an application that implements the following functionalities: 
(1) CRUD (create, read, update, delete) operations for an entity that represents a user, 
(2) a login operation, and 
(3) search operations (think for instance to three different search criteria).
	
The entity user must be defined as an association of multiple entities (e.g., the user entity, the address entity, and the preference entity).
It must also include the attribute �bestFriend� which is a possibly empty association with another user. Arbitrarily define attributes for the defined entities.

The implementation must be based on both Java and a framework implementing JPA, or alternatively on an OO language and a framework for ORM.

These functionalities should be implemented as being part of a large system, thus, extensively exploit separation of concerns when organizing the code. 
Make sure to write production quality code. In practice think to the case of a company that has asked you to submit this exercise to decide if hiring you.

## Getting Started

### Prerequisites

* [MySQL Server](https://dev.mysql.com/downloads/)
* Startare MySQL Server

### Create database

- Dal terminale eseguire il comando

```
mysql --host=localhost --user=root -p

```
- Inserire la password richiesta dell'utente root

- Creare un nuovo database

```

create database DATABASE_NAME;

```

### Clone the project

```
git clone -b dev2 https://federicopozzi33@bitbucket.org/federicopozzi33/assignment3-orm.git
```

### Settings

- Modificare il file __hibernate.cfg.xml__, inserendo negli appositi campi *DATABASE_NAME*, *USER* e *PASSWORD*

### Running the tests

- Per eseguire i test � sufficiente utilizzare la classe __T.java__, all'interno del package __test__
- Selezionare il file __T.java__, *Run As*, -> *JUnit Test*

## Built With

* [Hibernate](http://hibernate.org/) - Hibernate ORM
* [Maven](https://maven.apache.org/) - Dependency Management